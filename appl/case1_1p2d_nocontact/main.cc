// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Test for the elastic single-phase model coupled to a single-phase flow
 *        flow model in the facet domain, together with a mechanical model for
 *        the deformations and a contact problem on the fracture facets.
 * \note This test uses finite elemments for the mechanical deformations and
 *       the box scheme for flow.
 */
#include <config.h>
#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

// the type tags of the sub-problems
#include "properties_bulk_poromech.hh"
#include "properties_bulk_onep.hh"
#include "properties_facet_onep.hh"

#include <dumux/io/grid/gridmanager.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/linear/seqsolverbackend.hh>

#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>
#include <dumux/multidomain/fvgridgeometry.hh>
#include <dumux/multidomain/fvproblem.hh>
#include <dumux/multidomain/fvgridvariables.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>
#include <dumux/multidomain/facet/enrichedgridmanager.hh>
#include <dumux/multidomain/facet/vertexmapper.hh>
#include <dumux/multidomain/facet/geomechanics/couplingmanager_fem_nocontact.hh>
#include <dumux/multidomain/io/vtkoutputmodule.hh>

// The type tags of the bulk and facet flow problems
using BulkFlowTypeTag = Dumux::Properties::TTag::OnePBulkBox;
using FacetFlowTypeTag = Dumux::Properties::TTag::OnePFacetBox;

// obtain/define some types to be used below in the property definitions and in main
class TestTraits
{
    using BulkFlowFVG = Dumux::GetPropType<BulkFlowTypeTag, Dumux::Properties::GridGeometry>;
    using FacetFlowFVG = Dumux::GetPropType<FacetFlowTypeTag, Dumux::Properties::GridGeometry>;
    using BulkMechFVG = Dumux::GetPropType<Dumux::Properties::TTag::PoroElasticBulkFem, Dumux::Properties::GridGeometry>;

public:
    using MDTraits = Dumux::MultiDomainTraits< BulkFlowTypeTag,
                                               FacetFlowTypeTag,
                                               Dumux::Properties::TTag::PoroElasticBulkFem >;

    using CouplingMapperFlow = Dumux::FacetCouplingMapper<BulkFlowFVG, FacetFlowFVG>;
    using CouplingManager = Dumux::FacetCouplingPoroMechanicsCouplingManager<MDTraits, CouplingMapperFlow>;
};

// set the coupling manager property in the sub-problems
namespace Dumux {
namespace Properties {

template<class TypeTag>
struct CouplingManager<TypeTag, BulkFlowTypeTag> { using type = typename TestTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, FacetFlowTypeTag> { using type = typename TestTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::PoroElasticBulkFem> { using type = typename TestTraits::CouplingManager; };

} // end namespace Properties
} // end namespace Dumux

///////////////////
// main function //
///////////////////
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    Dune::MPIHelper::instance(argc, argv);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // the multidomain traits and domain indices
    using Traits = typename TestTraits::MDTraits;
    using CouplingManager = typename TestTraits::CouplingManager;
    constexpr auto bulkFlowId = CouplingManager::matrixFlowId;
    constexpr auto facetFlowId = CouplingManager::facetFlowId;
    constexpr auto bulkMechId = CouplingManager::mechanicsId;

    ///////////////////////////////////
    // Create the grid used for flow //
    ///////////////////////////////////
    using BulkFlowGrid = Traits::template SubDomain<bulkFlowId>::Grid;
    using FacetFlowGrid = Traits::template SubDomain<facetFlowId>::Grid;
    using GridManager = FacetCouplingGridManager<BulkFlowGrid, FacetFlowGrid>;
    GridManager gridManager;
    gridManager.init();
    gridManager.loadBalance();

    const auto& bulkGridView = gridManager.template grid<0>().leafGridView();
    const auto& facetGridView = gridManager.template grid<1>().leafGridView();

    ////////////////////////////////////////////////////
    // Create the grid used for the mechanical domain //
    ////////////////////////////////////////////////////
    using GridAdapter = CodimOneGridAdapter<typename GridManager::Embeddings>;
    using EnrichedVertexMapper = EnrichedVertexDofMapper<typename BulkFlowGrid::LeafGridView>;
    GridAdapter bulkFacetGridAdapter(gridManager.getEmbeddings());
    EnrichedVertexMapper vertexMapper(bulkGridView);

    // enrich the mapper to add degrees of freedoms on fractures
    vertexMapper.enrich(facetGridView, bulkFacetGridAdapter, true);

    // Create bulk grid where the fractures are "opened", thus interior boundaries
    using BulkFlowGridView = typename BulkFlowGrid::LeafGridView;
    using ElementMapper = Dune::MultipleCodimMultipleGeomTypeMapper<BulkFlowGridView>;
    ElementMapper elementMapper(bulkGridView, Dune::mcmgElementLayout());

    using MechanicalGrid = Traits::template SubDomain<bulkMechId>::Grid;
    using MechanicalGridView = typename MechanicalGrid::LeafGridView;
    EnrichedGridManager<MechanicalGrid> enrichedGridManager;
    enrichedGridManager.init(bulkGridView, elementMapper, vertexMapper);
    enrichedGridManager.loadBalance();

    const auto& mechGridView = enrichedGridManager.grid().leafGridView();


    ///////////////////////////////////////////////////////
    // All grids are built, make the grid geometries now //
    ///////////////////////////////////////////////////////
    using MDGridGeometry = MultiDomainFVGridGeometry<Traits>;
    MDGridGeometry fvGridGeometry;

    // standard construction for bulkFlow and facet flow
    fvGridGeometry.set(std::make_shared<typename MDGridGeometry::template Type<bulkFlowId>>(bulkGridView), bulkFlowId);
    fvGridGeometry.set(std::make_shared<typename MDGridGeometry::template Type<facetFlowId>>(facetGridView), facetFlowId);

    // FEM-type construction for mechanical sub-domain
    using MechSpaceBasis = typename MDGridGeometry::template Type<bulkMechId>::AnsatzSpaceBasis;
    auto mechSpaceBasis = std::make_shared<MechSpaceBasis>(mechGridView);
    fvGridGeometry.set(std::make_shared<typename MDGridGeometry::template Type<bulkMechId>>(mechSpaceBasis), bulkMechId);

    // update the grid geometries
    fvGridGeometry[bulkMechId].update();
    fvGridGeometry[facetFlowId].update();
    fvGridGeometry[bulkFlowId].update(facetGridView, bulkFacetGridAdapter);

    // the coupling manager
    using CouplingManager = typename TestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();


    ////////////////////////////////////
    // Instantiate the (sub-)problems //
    ////////////////////////////////////
    MultiDomainFVProblem<Traits> problem;

    using BulkFlowProblem = MultiDomainFVProblem<Traits>::template Type<bulkFlowId>;
    using FacetFlowProblem = MultiDomainFVProblem<Traits>::template Type<facetFlowId>;
    using BulkElasticProblem = MultiDomainFVProblem<Traits>::template Type<bulkMechId>;

    auto bulkFlowSpatialParams = std::make_shared<typename BulkFlowProblem::SpatialParams>(fvGridGeometry.get(bulkFlowId), couplingManager, "OnePBulk");
    auto facetFlowSpatialParams = std::make_shared<typename FacetFlowProblem::SpatialParams>(fvGridGeometry.get(facetFlowId), couplingManager, "OnePFacet");
    auto bulkElasticSpatialParams = std::make_shared<typename BulkElasticProblem::SpatialParams>(fvGridGeometry.get(bulkMechId), "ElasticBulk");

    problem.set(std::make_shared<BulkFlowProblem>(fvGridGeometry.get(bulkFlowId), bulkFlowSpatialParams, couplingManager, "OnePBulk"), bulkFlowId);
    problem.set(std::make_shared<FacetFlowProblem>(fvGridGeometry.get(facetFlowId), facetFlowSpatialParams, couplingManager, "OnePFacet"), facetFlowId);
    problem.set(std::make_shared<BulkElasticProblem>(fvGridGeometry.get(bulkMechId), bulkElasticSpatialParams, couplingManager, "ElasticBulk"), bulkMechId);

    ////////////////////////////////////////////////
    // Make the coupling maps between the domains //
    ////////////////////////////////////////////////

    // the coupling mapper for flow
    using CouplingMapperFlow = typename TestTraits::CouplingMapperFlow;
    auto couplingMapperFlow = std::make_shared<CouplingMapperFlow>();
    couplingMapperFlow->update(fvGridGeometry[bulkFlowId], fvGridGeometry[facetFlowId], gridManager.getEmbeddings());

    // set up index maps between bulk flow and mechanical elements
    std::vector<std::size_t> mechInsertionToElemIdx(fvGridGeometry[bulkMechId].gridView().size(0));
    for (const auto& element : elements(fvGridGeometry[bulkMechId].gridView()))
    {
        const auto insIdx = enrichedGridManager.insertionIndex(element);
        const auto eIdx = fvGridGeometry[bulkMechId].elementMapper().index(element);
        mechInsertionToElemIdx[insIdx] = eIdx;
    }

    std::vector<std::size_t> bulkFlowToMechIdx(fvGridGeometry[bulkFlowId].gridView().size(0));
    std::vector<std::size_t> mechToBulkFlowIdx(fvGridGeometry[bulkMechId].gridView().size(0));
    for (const auto& element : elements(fvGridGeometry[bulkFlowId].gridView()))
    {
        const auto eIdx = fvGridGeometry[bulkFlowId].elementMapper().index(element);
        const auto insIdx = enrichedGridManager.elementInsertionIndex(eIdx);
        const auto mechElemIdx = mechInsertionToElemIdx[insIdx];

        bulkFlowToMechIdx[eIdx] = mechElemIdx;
        mechToBulkFlowIdx[mechElemIdx] = eIdx;
    }

    using BulkIndexMap = FacetCouplingPoroMechIndexMap<bulkMechId, bulkFlowId>;
    BulkIndexMap bulkIndexMap(std::move(mechToBulkFlowIdx), std::move(bulkFlowToMechIdx));

    // the solution vector
    typename Traits::SolutionVector x;

    // initialize the coupling manager, it receives all problems and the coupling maps!
    couplingManager->init(problem.get(bulkFlowId), problem.get(facetFlowId), problem.get(bulkMechId),
                          couplingMapperFlow, bulkIndexMap, x);

    // initialize the solution vector
    problem.applyInitialSolution(x);
    auto xOld = x;

    // initialize the grid variables with the initial solution
    using GridVariables = MultiDomainFVGridVariables<Traits>;
    GridVariables gridVars(fvGridGeometry.getTuple(), problem.getTuple());
    gridVars.init(x);

    ///////////////////////////
    // Initialize VTK output //
    ///////////////////////////

    using BulkFlowGridVariables = typename GridVariables::template Type<bulkFlowId>;
    using FacetFlowGridVariables = typename GridVariables::template Type<facetFlowId>;

    using BulkFlowVtkOutputModule = VtkOutputModule<BulkFlowGridVariables, typename Traits::template SubDomain<bulkFlowId>::SolutionVector>;
    using FacetFlowVtkOutputModule = VtkOutputModule<FacetFlowGridVariables, typename Traits::template SubDomain<facetFlowId>::SolutionVector>;

    static constexpr bool bulkFlowIsBox = MDGridGeometry::template Type<bulkFlowId>::discMethod == DiscretizationMethod::box;
    const auto bulkOutputType = bulkFlowIsBox ? Dune::VTK::nonconforming : Dune::VTK::conforming;
    BulkFlowVtkOutputModule bulkFlowVtkWriter(gridVars[bulkFlowId], x[bulkFlowId], problem[bulkFlowId].name(), "", bulkOutputType);
    FacetFlowVtkOutputModule facetFlowVtkWriter(gridVars[facetFlowId], x[facetFlowId], problem[facetFlowId].name());

    // add additional output
    problem[bulkFlowId].addOutputFields(bulkFlowVtkWriter);
    facetFlowVtkWriter.addField(problem[facetFlowId].apertures(), "aperture", FacetFlowVtkOutputModule::FieldType::element);
    facetFlowVtkWriter.addField(problem[facetFlowId].permeabilities(), "permeability", FacetFlowVtkOutputModule::FieldType::element);

    using BulkFlowIOFields = GetPropType<typename Traits::template SubDomain<bulkFlowId>::TypeTag, Properties::IOFields>;
    using FacetFlowIOFields = GetPropType<typename Traits::template SubDomain<facetFlowId>::TypeTag, Properties::IOFields>;
    BulkFlowIOFields::initOutputModule(bulkFlowVtkWriter);
    FacetFlowIOFields::initOutputModule(facetFlowVtkWriter);

    // velocity output for the flow sub-problems
    using BulkFlowTypeTag = typename Traits::template SubDomain<bulkFlowId>::TypeTag;
    using FacetFlowTypeTag = typename Traits::template SubDomain<facetFlowId>::TypeTag;

    //////////////////////////////////////
    // VTK output for mechanical domain
    using Vector = Dune::FieldVector<double, MechanicalGridView::dimension>;
    using MechElementMapper = typename MDGridGeometry::template Type<bulkMechId>::ElementMapper;
    using P0MechanicsFunction = Vtk::VectorP0VTKFunction<MechanicalGridView, MechElementMapper, std::vector<Vector>>;
    auto mechWriter = std::make_shared<Dune::VTKWriter<MechanicalGridView>>(mechGridView);
    Dune::VTKSequenceWriter<MechanicalGridView> mechSequenceWriter(mechWriter, getParamFromGroup<std::string>("ElasticBulk", "Problem.Name"));

    auto gfU = Dune::Functions::makeDiscreteGlobalBasisFunction<Vector>(*mechSpaceBasis, x[bulkMechId]);
    auto sigmaXFunction = std::make_shared<P0MechanicsFunction>(mechGridView, fvGridGeometry[bulkMechId].elementMapper(), problem[bulkMechId].sigma_x(), "Sigma_x", 2);
    auto sigmaYFunction = std::make_shared<P0MechanicsFunction>(mechGridView, fvGridGeometry[bulkMechId].elementMapper(), problem[bulkMechId].sigma_y(), "Sigma_y", 2);

    mechWriter->addVertexData(gfU, Dune::VTK::FieldInfo("u", Dune::VTK::FieldInfo::Type::vector, 2));
    mechWriter->addCellData(sigmaXFunction);
    mechWriter->addCellData(sigmaYFunction);

    ///////////////////////
    // Solve the problem //
    ///////////////////////

    // the assembler
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( problem.getTuple(), fvGridGeometry.getTuple(), gridVars.getTuple(), couplingManager);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

    ///////////////////////////
    // The actual solve step
    newtonSolver->solve(x);
    gridVars.update(x);


    //////////////////////
    // Write VTK output //
    //////////////////////

    // update additional output
    problem[bulkFlowId].updateOutputFields(x[bulkFlowId]);
    problem[facetFlowId].updateOutputFields(x[facetFlowId]);

    // update stresses
    problem[bulkMechId].updateOutputFields(gridVars[bulkMechId], *assembler, x[bulkMechId], bulkMechId);

    // write vtk output
    bulkFlowVtkWriter.write(1.0);
    facetFlowVtkWriter.write(1.0);
    mechSequenceWriter.write(1.0);

    // print average pressure to screen
    const auto pAvg = problem[bulkFlowId].computeAveragePressure(x[bulkFlowId]);
    std::cout << "Average pressure: " << pAvg << std::endl;
    std::ofstream pressureFile("avgpressure.log", std::ios::out);
    pressureFile << pAvg << std::endl;

    // print parameter usage and exit
    Parameters::print();
    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (std::exception& e)
{
    std::cerr << "Standard exception: " << e.what() << " ---> Abort!" << std::endl;
    return 4;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 5;
}
