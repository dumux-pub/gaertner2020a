[Problem] # general problem parameters
EnableGravity = false
InjectionRate = 1e-12 # in kg/s/m2
TunnelRadius = 1.4     # must match the value used in the .geo file!

[1.Component] # the component defining the solid matrix
SolidDensity = 2750.0

[0.Component] # the component defining the pore fluid
LiquidKinematicViscosity = 1e-6
LiquidDensity = 1000

[Fractures] # some properties of the fractures
InitialGap = 1e-3
MinHydraulicAperture = 1e-8
ZeroGapThreshold = 1e-10

[Grid] # grid used in bulk flow&mech and facet flow domains
File = ./../grids/tunnel_section.msh

[Contact.Grid] # grid used for the contact problem (must be one level coarser)
File = ./../grids/tunnel_section_contact.msh

[SpatialParams] # common parameters used in several sub-problems
InitialPorosity = 0.11 #NAB17-051 p.8 Brown Dogger/used from both ElasticBulk & OnePBulk
BentonitePorosity = 0.4 #NAB17-009 p.7

[OnePBulk]
Problem.Name = onep_bulk
SpatialParams.InitialPermeability = 1e-21 #NAB13-051 p.41 Brown Dogger
SpatialParams.BentonitePermeability = 5e-20 #NAB17-009 p.41
SpatialParams.UseConstantPorosity = false
SpatialParams.UseConstantPermeability = false

[OnePFacet]
Problem.Name = onep_facet

[Contact]
Problem.Name = contact
InitialTraction = 0.0 0.0
PenaltyFactor = 1e8
FrictionCoefficient = 0.5
Assembly.FEIntegrationOrder = 2

[ElasticBulk]
Problem.Name = elastic_bulk
SpatialParams.EModule = 1e10 #NAB13-78 p.15
SpatialParams.Nu = 0.3 #NAB13-78 p.15
SpatialParams.BiotCoefficient = 1.0
Assembly.FEIntegrationOrder = 2

[Newton]
MaxSteps = 40
SatisfyResidualAndShiftCriterion = false
EnableAbsoluteResidualCriterion = true
EnableResidualCriterion = false
EnableShiftCriterion = true
ResidualReduction = 1e-20
MaxAbsoluteResidual = 5.0e-6
MaxRelativeShift = 1e-8
UseLineSearch = false

[Vtk]
OutputName = case1_1p2d
