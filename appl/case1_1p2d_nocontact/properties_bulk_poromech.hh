// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The propertiies for the poromechanical domain in the elastic
 *        single-phase facet coupling test.
 */
#ifndef DUMUX_GAERTNER2020_CASE1_BULK_POROMECH_PROPERTIES_HH
#define DUMUX_GAERTNER2020_CASE1_BULK_POROMECH_PROPERTIES_HH

#include <dune/alugrid/grid.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dumux/geomechanics/poroelastic/model.hh>

#include <dumux/discretization/fem.hh>
#include <dumux/discretization/fem/ipdata.hh>
#include <dumux/discretization/fem/elementsolution.hh>
#include <dumux/discretization/fem/fegridgeometry.hh>
#include <dumux/multidomain/facet/box/properties.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include "problem_bulk_poromech.hh"
#include "spatialparams_bulk_poromech.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {

struct PoroElasticBulk { using InheritsFrom = std::tuple<PoroElastic>; };
struct PoroElasticBulkFem { using InheritsFrom = std::tuple<PoroElasticBulk, FiniteElementModel>; };

} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::PoroElasticBulk> { using type = Dune::ALUGrid<2, 2, Dune::simplex, Dune::nonconforming>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::PoroElasticBulk> { using type = Dumux::PoroElasticSubProblem<TypeTag>; };

// The fluid phase consists of one constant component
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::PoroElasticBulk>
{
private:
    // we use a constant component and use index 1
    // (index 0 is taken for the liquid component)
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Component = Dumux::Components::Constant<1, Scalar>;

public:
    using type = Dumux::FluidSystems::OnePLiquid< Scalar, Component >;
};
// The spatial parameters property
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::PoroElasticBulk>
{
    using type = PoroElasticSpatialParams< GetPropType<TypeTag, Properties::Scalar>,
                                           GetPropType<TypeTag, Properties::GridGeometry> >;
};

// We use a lagrange basis of first order here
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::PoroElasticBulkFem>
{
private:
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using FEBasis = Dune::Functions::LagrangeBasis<GridView, 1>;
public:
    using type = FEGridGeometry<FEBasis>;
};

} // end namespace Dumux::Properties

#endif
