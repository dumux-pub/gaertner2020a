import matplotlib.pyplot as plt
import numpy as np
import subprocess
import sys
import os

sys.path.append( os.path.join(os.getcwd(), "../grids") )

# define a function to exit with error message
def printErrorAndExit(errorMessage):
    sys.stderr.write(errorMessage)
    sys.exit(1)

# execute dumux for varying horizontal fracture extent
# and plot average tunnel pressure as function of that extent
nMin = 0     # minimum horizontal extent to check
nMax = 4     # maximum horizontal extent to check
numSamples = 5 # how many sample points, i.e. simulations. Determines number of data points between rMin and rMax
nArray = np.linspace(nMin, nMax, numSamples)

# E modules to check
eModules = [1e10]

# initial gap to use
initGap = 1e-4

#order of colors
c = []
c.append('tab:blue')
c.append('tab:orange')
c.append('tab:purple')
c.append('tab:gray')
c.append('tab:pink')
c.append('tab:brown')

#counter
a = 0

# make sure the executable is up to date
try: subprocess.run(['make', 'case2_2p2d_nocontact'], check=True)
except: printErrorAndExit("Could not build executable\n")

# remove potential preexisting average pressure value
if os.path.exists("avgpressure.log"):
    subprocess.call(['rm', 'avgpressure.log'])
    print("Removed old pressure log file!\n")

for eModule in eModules:
    for n in nArray:

        # body of the temporary grid file name
        gridFileNameBody = os.path.join(os.getcwd(), "../grids/temp")

        # prepare the .geo file
        try: subprocess.run(['python3', './../grids/fractures_struct_case2_n.py', gridFileNameBody + ".geo", str(n)], check=True)
        except: printErrorAndExit("Could not create .geo file\n")

        # prepare the grid(s), '2' is there because this case considers a 2d setting
        try: subprocess.run(['python3', './../grids/preparegrid.py', gridFileNameBody + ".geo", '2'], check=True)
        except: printErrorAndExit("Could not create meshes\n")

        process = './case2_2p2d_nocontact'
        if(n==0):
            process = './case2_2p2d_nofractures'
            
        # run dumux
        
        try: subprocess.run([process, 'params.input', '-Grid.File', gridFileNameBody + ".msh", '-ElasticBulk.SpatialParams.EModule', str(eModule), '-Fractures.InitialGap', str(initGap), '-Vtk.OutputName','case2_2p2d_n_'+ str(n) , '-ElasticBulk.Problem.Name', 'elastic_bulk_n_'+str(n)], check=True)
        #'case2_2p2d_vs_n'+ str(n)
        #'elastic_bulk_vs_n'+str(n)
        except: printErrorAndExit("Could not run dumux\n")

        data = np.loadtxt('avgpressure.log', delimiter=',')

        # add result to figure
        plt.figure(1)
        plt.plot(data[:,0], data[:,1], label="pw, n = "+str(n)+", E = {:.2e} Pa".format(eModule), color = c[a])
        
        plt.figure(2)
        plt.plot(data[:,0], data[:,2], label="pn, n = "+str(n)+", E = {:.2e} Pa".format(eModule), color = c[a])
        a+=1

plt.figure(1)
plt.xlabel("t [s]")
plt.ylabel("p [Pa]")
plt.legend()

plt.savefig("pw_t_n.pdf", bbox_inches='tight')

plt.figure(2)
plt.xlabel("t [s]")
plt.ylabel("p [Pa]")
plt.legend()

plt.savefig("pn_t_n.pdf", bbox_inches='tight')
