// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The properties for the (d-1)-dimensional facet domain in the elastic
 *        two-phase facet coupling test.
 */
#ifndef DUMUX_GAERTNER2020_CASE2_FACET_FLOW_PROPERTIES_HH
#define DUMUX_GAERTNER2020_CASE2_FACET_FLOW_PROPERTIES_HH

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/h2.hh>
#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>

#include <dumux/porousmediumflow/2p/model.hh>

#include "problem_facet_twop.hh"
#include "spatialparams_facet_twop.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {

struct TwoPFacet { using InheritsFrom = std::tuple<TwoPNI>; };
struct TwoPFacetBox { using InheritsFrom = std::tuple<TwoPFacet, BoxModel>; };

} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPFacet> { using type = Dune::FoamGrid<1, 2>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPFacet> { using type = TwoPFacetProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPFacet>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    using type = TwoPFacetSpatialParams<GridGeometry, Scalar, CouplingManager>;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPFacet>
{
private:
    // we use a constant component here and give it index 0
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2O = Components::SimpleH2O<Scalar>;
    using H2 = Components::H2<Scalar>;

    using LiquidPhase = FluidSystems::OnePLiquid<Scalar, H2O>;
    using GasPhase = FluidSystems::OnePGas<Scalar, H2>;
public:
    using type = FluidSystems::TwoPImmiscible< Scalar, LiquidPhase, GasPhase >;
};

} // end namespace Dumux::Properties

#endif
