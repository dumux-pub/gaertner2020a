// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The properties for the bulk domain in the elastic
 *        two-phase facet coupling test.
 */
#ifndef DUMUX_GAERTNER2020_CASE2_BULK_FLOW_PROPERTIES_HH
#define DUMUX_GAERTNER2020_CASE2_BULK_FLOW_PROPERTIES_HH

// Preprocessor variable to make this header work with both
// fractures being considered and neglected
#ifndef FRACTURESCONSIDERED
#define FRACTURESCONSIDERED 1
#endif

// enables compatibiility with models without mechanics
#ifndef MECHANICSCONSIDERED
#define MECHANICSCONSIDERED 1
#endif

#include <dune/alugrid/grid.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/h2.hh>
#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>

#include <dumux/discretization/box.hh>
#include <dumux/multidomain/facet/box/properties.hh>
#include <dumux/porousmediumflow/2p/model.hh>

#include "problem_bulk_twop.hh"
#include "spatialparams_bulk_twop.hh"

namespace Dumux::Properties {

// create the type tag nodes
namespace TTag {
struct TwoPBulk { using InheritsFrom = std::tuple<TwoPNI>; };
#if FRACTURESCONSIDERED
struct TwoPBulkBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, TwoPBulk>; };
#else
struct TwoPBulkBox { using InheritsFrom = std::tuple<TwoPBulk, BoxModel>; };
#endif
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPBulk> { using type = Dune::ALUGrid<2, 2, Dune::simplex, Dune::nonconforming>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPBulk> { using type = TwoPBulkProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPBulk>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
#if MECHANICSCONSIDERED
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
public:
    using type = TwoPBulkSpatialParams<GridGeometry, Scalar, CouplingManager>;
#else
public:
    using type = TwoPBulkSpatialParams<GridGeometry, Scalar>;
#endif
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPBulk>
{
private:
    // we use a constant component here and give it index 0
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2O = Components::SimpleH2O<Scalar>;
    using H2 = Components::H2<Scalar>;

    using LiquidPhase = FluidSystems::OnePLiquid<Scalar, H2O>;
    using GasPhase = FluidSystems::OnePGas<Scalar, H2>;
public:
    using type = FluidSystems::TwoPImmiscible< Scalar, LiquidPhase, GasPhase >;
};

} // end namespace Dumux::Properties

#endif
