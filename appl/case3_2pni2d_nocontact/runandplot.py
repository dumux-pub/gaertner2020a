import matplotlib.pyplot as plt
import numpy as np
import subprocess
import sys
import os

sys.path.append( os.path.join(os.getcwd(), "../grids") )

# define a function to exit with error message
def printErrorAndExit(errorMessage):
    sys.stderr.write(errorMessage)
    sys.exit(1)

# execute dumux for varying horizontal fracture extent
# and plot average tunnel pressure as function of that extent
rMin = 2.0     # minimum horizontal extent to check
rMax = 3.5     # maximum horizontal extent to check
numSamples = 4 # how many sample points, i.e. simulations. Determines number of data points between rMin and rMax
rArray = np.linspace(rMin, rMax, numSamples)

# E modules to check
eModules = [1e10, 1e11]

# initial gap to use
initGap = 1e-7

# make sure the executable is up to date
try: subprocess.run(['make', 'case1_1p2d_nocontact'], check=True)
except: printErrorAndExit("Could not build executable\n")

# remove potential preexisting average pressure value
if os.path.exists("avgpressure.log"):
    subprocess.call(['rm', 'avgpressure.log'])
    print("Removed old pressure log file!\n")

for eModule in eModules:
    avgPressures = []
    for r in rArray:

        # body of the temporary grid file name
        gridFileNameBody = os.path.join(os.getcwd(), "../grids/temp")

        # prepare the .geo file
        try: subprocess.run(['python3', './../grids/fractures_struct_varinput.py', gridFileNameBody + ".geo", str(r)], check=True)
        except: printErrorAndExit("Could not create .geo file\n")

        # prepare the grid(s), '2' is there because this case considers a 2d setting
        try: subprocess.run(['python3', './../grids/preparegrid.py', gridFileNameBody + ".geo", '2'], check=True)
        except: printErrorAndExit("Could not create meshes\n")

        # run dumux
        try: subprocess.run(['./case1_1p2d_nocontact', 'params.input',
                             '-Grid.File', gridFileNameBody + ".msh",
                             '-Fractures.InitialGap', str(initGap), '-SpatialParams.EModule', str(eModule)], check=True)
        except: printErrorAndExit("Could not run dumux\n")

        # read in the computed average pressure
        if not os.path.exists("avgpressure.log"):
            printErrorAndExit("Could not find file with average pressure data")
        avgPressures.append( float(np.loadtxt("avgpressure.log")) )

    # add result to figure
    plt.figure(1)
    plt.plot(rArray, avgPressures, label="E = {:.2e}".format(eModule))
    plt.xlabel("r [m]")
    plt.ylabel("p [Pa]")
    plt.legend()

plt.savefig("plot.pdf", bbox_inches='tight')
