import numpy as np
import math
import random
import sys

# function to determine if two lines intersect
def doIntersect(segment1, segment2):

    p11 = segment1[0]
    p12 = segment1[1]
    p21 = segment2[0]
    p22 = segment2[1]

    #slope of segments
    slope1 = (p11[1]-p12[1])/(p11[0]-p12[0])
    slope2 = (p21[1]-p22[1])/(p21[0]-p22[0])

    #parallel segments
    if(slope1 == slope2):
        return False

    #ordinates
    b1 = p11[1]-p11[0]*slope1
    b2 = p21[1]-p21[0]*slope2

    #intersection point
    Xi = (b2-b1)/(slope1-slope2)
    Yi = slope1 * Xi +b1

    #determine if lines intersect
    if ( (Xi < max( min(p11[0],p12[0]), min(p21[0],p22[0]) )) or
        (Xi > min( max(p11[0],p12[0]), max(p21[0],p22[0]) )) ):
        return False
    else:
        return slope1, slope2, Xi, Yi

# function to compute the intersection angle between two segments
def intersectionAngle(segment1, segment2, slope1, slope2):

    #compute intersection angle
    phi = math.degrees(math.atan((slope2-slope1)/(1+(slope2*slope1))))
    return abs(phi)

# function to compute the distance of a point to a segment
def computeDistance(segment, point):

    # project the point on the supporting line
    v1 = segment[1] - segment[0]
    v2 = point - segment[0]

    sp = v1.dot(v2)
    normV1 = np.linalg.norm(v1)

    # projection lies "behind" segment[0]
    if sp < 0.0:
        return np.linalg.norm(v2)

    # length on line from segment[0] to projection point
    length = sp/normV1
    if length > normV1: # projection lies outside the segment
        return np.linalg.norm(point - segment[1])

    # compute projection point
    v1 *= length/normV1
    projection = segment[0] + v1
    return np.linalg.norm(point - projection)

###########################
# main body of the script #
###########################

# get file name to write the result in
if len(sys.argv) != 2:
    sys.stderr.write("Please provide input argument - the target .geo file")
    sys.exit(1)

targetFile = open(sys.argv[1], 'w')
targetNumFracs = 100# target number of fractures
b = 20              # width of the domain
h = 20              # height of the domain
r = 1.4             # tunnel radius
cr = 0.525            # canister radius
flMin = 0.2        # minimum fracture length
flMax = 1.5        # maximum fracture length

minIntersectionAngle = math.degrees(math.pi/20.0) # minimum allowed intersection angle
minDistance = r*0.03               # minimum distance allowed

fractures = []
while len(fractures) < targetNumFracs:

    keepSampling = True
    while keepSampling:
        # first point
        phi = random.uniform(0, math.pi/2.0)
        s = -r/math.pi * phi + r #reduces standard deviation of fracture density for upper host rock
        r1 = random.gauss(r, s)
        x = math.cos(phi)*r1
        y = math.sin(phi)*r1
        p1 = np.array([x, y])

        # second point
        # sample candidate for p2
        angle = math.radians(random.gauss(0.0, 20.0))
        # normalized sample direction vector
        dir = np.array([np.sign(x),-1.0* np.sign(angle)*angle])
        dir /= np.linalg.norm(dir)

        # sample candidate
        p2 = p1 + dir*random.uniform(flMin, flMax)

        # segment candidate
        segment = [p1, p2]

        # check constraints
        keepSampling = False

        # # avoid intersection with tunnels edge
        # if (math.sqrt(p1[0]**2+p1[1]**2)<r or math.sqrt(p2[0]**2+p2[1]**2)<r+1e-6):
        #     keepSampling = True
        #     continue

        # avoid segments of which only a small portion is outside the tunnel
        for pointPair in [[p1, p2], [p2, p1]]:
            if (np.linalg.norm(p1) < r
                and np.linalg.norm(p2) > r
                and np.linalg.norm(p2) - r < r/3.0):
               keepSampling = True
               break

        if keepSampling: continue

        # avoid small intersection angles & distances to bottom boundary
        bottomSegment = [np.array([0.0, 0.0]), np.array([b, 0.0])]
        for boundary in [bottomSegment]:
            if doIntersect(boundary, segment) != False:
                slope1,slope2, Xi, Yi = doIntersect(boundary, segment)
                if (intersectionAngle(boundary, segment, slope1, slope2) < minIntersectionAngle):
                    keepSampling = True
                    break

            if (computeDistance(boundary, segment[0]) < minDistance
                or computeDistance(boundary, segment[1]) < minDistance):
                keepSampling = True
                break

        if keepSampling: continue

        #variable to count intersections
        a = 0
        intersections = []

        # constraints to other fracture segments
        for fracSegment in fractures:

            # avoid small angles and compute intersection points
            if doIntersect(fracSegment, segment) != False:
                slope1,slope2, Xi, Yi = doIntersect(fracSegment, segment)
                intersect = [Xi, Yi]
                intersections.append(intersect)
                a+=1
                if (intersectionAngle(fracSegment, segment, slope1, slope2) < minIntersectionAngle):
                    keepSampling = True
                    break

            # avoid small distances
            if (computeDistance(segment, fracSegment[0]) < minDistance
                or computeDistance(segment, fracSegment[1]) < minDistance
                or computeDistance(fracSegment, segment[0]) < minDistance
                or computeDistance(fracSegment, segment[1]) < minDistance):
                keepSampling = True
                break

        # avoid small distances between intersection points
        d = 0
        c = d+1
        if (a >= 2):
            while (d < a-1):
                while (c <= a-1):
                    if(math.sqrt((intersections[d][0] - intersections[c][0])**2 +(intersections[d][1] - intersections[c][1])**2) <= minDistance):
                            keepSampling = True
                            break
                    c+=1
                d+=1
                c = d+1
            continue

    # an admissile segment has been found
    fractures.append(segment)
    print("Found admissible fracture number " + str(len(fractures)))

# write the remainder of the .geo file
targetFile.write("SetFactory(\"OpenCASCADE\"); // Use OpenCascade for boolean operations\n")
targetFile.write("Mesh.Format = 1;             // Use Gmsh mesh format\n\n")

targetFile.write("/////////////////////\n")
targetFile.write("// Inputs\n")

targetFile.write("x = " + str(b)   + "; // x extent of the domain (in meter)\n")
targetFile.write("y = " + str(h)   + "; // y extent of the domain (in meter)\n")
targetFile.write("r = " + str(r)   + "; // tunnel radius (in meter)\n")
targetFile.write("cr = " + str(cr) + "; // canister radius (in meter)\n")

targetFile.write("size_f = " + str(r/4.5) + "; // mesh size on fractures\n")
targetFile.write("size_t = " + str(r/3.5) + "; // mesh size inside the tunnel\n")
targetFile.write("size_d = " + str(r*2) + ";     // mesh size in domain\n\n")

# write the generated lines into the .geo file
for i in range(0, len(fractures)):
    p1 = fractures[i][0]
    p2 = fractures[i][1]
    p1Idx = i*2 + 1
    p2Idx = i*2 + 2
    targetFile.write("Point(" + str(p1Idx) + ") = {" + str(p1[0]) + ", " + str(p1[1]) + ", 0.0};\n")
    targetFile.write("Point(" + str(p2Idx) + ") = {" + str(p2[0]) + ", " + str(p2[1]) + ", 0.0};\n")
    targetFile.write("Line(" + str(i+1) + ") = {" + str(p1Idx) + ", " + str(p2Idx) + "};\n\n")

targetFile.write("///////////////////////////////////////\n")
targetFile.write("// Process input into final geometry\n\n")

targetFile.write("// describing the tunnel\n\n")
targetFile.write("p1 = newp; Point(p1) = {0.0, 0.0, 0.0};\n")
targetFile.write("p2 = newp; Point(p2) = {r, 0.0, 0.0};\n")
targetFile.write("p3 = newp; Point(p3) = {0.0, r, 0.0};\n\n")

targetFile.write("l1 = newl; Line(l1) = {p1, p2};\n")
targetFile.write("l2 = newl; Line(l2) = {p3, p1};\n")
targetFile.write("tunnel = newc; Circle(tunnel) = {p2, p1, p3};\n")
targetFile.write("ll1 = newll; Line Loop(ll1) = {l1, tunnel, l2};\n")
targetFile.write("tunnelId = news; Plane Surface(tunnelId)={ll1};\n\n")

targetFile.write("//describing the canisiter\n")
targetFile.write("p4 = newp; Point(p4) = {cr, 0.0, 0.0};\n")
targetFile.write("p5 = newp; Point(p5) = {0, cr, 0};\n\n")

targetFile.write("l3 = newl; Line(l3) = {p1,p4};\n")
targetFile.write("l4 = newl; Line(l4) = {p5,p1};\n")
targetFile.write("canister = newc; Circle(canister) = {p4,p1,p5};\n")
targetFile.write("ll2 = newll; Line Loop(ll2) = {l3, canister, l4};\n")
targetFile.write("canisterId = news; Plane Surface(canisterId)={ll2};\n\n")

targetFile.write("// construct the actual domain by cutting out the tunnel from a rectangle\n")
targetFile.write("rectId   = news; Rectangle(rectId) = {0, 0, 0, x, y, 0}; // domain outline\n")
targetFile.write("domainId = news; BooleanDifference(domainId) = { Surface{rectId}; Delete; }{ Surface{tunnelId}; };\n\n")

targetFile.write("hollowTunnelId = news; BooleanDifference(hollowTunnelId) = { Surface{tunnelId}; Delete; }{ Surface{canisterId}; Delete; };\n\n")

targetFile.write("// cut all lines to be confined by the domain\n")
targetFile.write("confinedLines[] = BooleanIntersection{ Line{1:" + str(len(fractures)) + "}; Delete; }{ Surface{domainId}; };\n\n")

targetFile.write("// fragment the resulting lines to detect mutual intersections\n")
targetFile.write("fragmentedLines[] = BooleanFragments{ Line{confinedLines[]}; Delete; }{ Line{confinedLines[]}; Delete; };\n\n")

targetFile.write("// make the union of the tunnel and the domain\n")
targetFile.write("domainSurfs[] = BooleanUnion{ Surface{:}; Delete; }{ Surface{:}; Delete; };\n\n")

targetFile.write("// fragment the domain by the lines\n")
targetFile.write("BooleanFragments{ Surface{domainSurfs[]}; Delete; }{ Line{fragmentedLines[]}; }\n\n")

targetFile.write("// set mesh size specs\n")
targetFile.write("Characteristic Length{ Point{:} } = size_d;\n")
targetFile.write("Characteristic Length{ PointsOf{Surface{hollowTunnelId};} } = size_t;\n\n")
targetFile.write("Characteristic Length{ PointsOf{Line{fragmentedLines[]};} } = size_f;\n\n")

targetFile.write("/////////////////////////\n")
targetFile.write("// set physical indices\n")
targetFile.write("If (gridDim == 2) // omit 2d elements if 1d grid is desired\n")
targetFile.write("    Physical Surface(2) = {Surface{:}};\n")
targetFile.write("EndIf\n")
targetFile.write("Physical Line(2) = {fragmentedLines[]};\n")
