SetFactory("OpenCASCADE"); // Use OpenCascade for boolean operations
Mesh.Format = 1;           // Use Gmsh mesh format

/////////////////////
// Inputs

x = 20; // x extent of the domain (in meter)
y = 20; // y extent of the domain (in meter)
r = 1.4;  // tunnel radius (in meter)

size_f = r/3.0; // mesh size on fractures
size_d = r;     // mesh size in domain

// define a few fracture lines
// TODO: Outsource this part into a script randomly generates
//       the fracture lines and generates the .geo file
//Point(1) = {0, 0, 0.0};
//Point(2) = {+2.0*r, r, 0.0};
//Line(1) = {1, 2};

//Point(3) = {0, -2.0*r, 0.0};
//Point(4) = {0, 2.0*r, 0.0};
//Line(2) = {3, 4};

Merge "fractures.geo";

///////////////////////////////////////
// Process input into final geometry

// construct the actual domain by cutting out the tunnel from a rectangle
rectId   = news; Rectangle(rectId) = {-x/2.0, -y/2.0, 0, x, y, 0}; // domain outline
tunnelId = news; Disk(tunnelId) = {0, 0, 0, r, r};               // disk desribing the tunnel
domainId = news; BooleanDifference(domainId) = { Surface{rectId}; Delete; }{ Surface{tunnelId}; Delete; };

// cut all lines to be confined by the domain
confinedLines[] = BooleanIntersection{ Line{1:10}; Delete; }{ Surface{domainId}; };

// fragment the resulting lines to detect mutual intersections
fragmentedLines[] = BooleanFragments{ Line{confinedLines[]}; Delete; }{ Line{confinedLines[]}; Delete; };

// fragment the domain by the lines
BooleanFragments{ Surface{domainId}; Delete; }{ Line{fragmentedLines[]}; }

// set mesh size specs
Characteristic Length{ Point"*" } = size_d;
Characteristic Length{ PointsOf{Line{fragmentedLines[]};} } = size_f;

/////////////////////////
// set physical indices
If (gridDim == 2) // omit 2d elements if 1d grid is desired
    Physical Surface(2) = {Surface{:}};
EndIf

Physical Line(2) = {fragmentedLines[]};
