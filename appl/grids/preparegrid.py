import os
import sys
import subprocess

if len(sys.argv) != 3:
    sys.stderr.write("Please provide a .geo file and the dimension of the grid as inputs")
    sys.exit(1)

gmshFile = sys.argv[1]
dim = sys.argv[2]

# create mesh
subprocess.call(['gmsh', '-' + dim,
                 '-setnumber', 'gridDim', dim,
                 '-format', 'msh2', gmshFile])



