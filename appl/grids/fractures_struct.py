import numpy as np
import math
import random
import sys

# get file name to write the result in
if len(sys.argv) != 2:
    sys.stderr.write("Please provide input argument - the target .geo file")
    sys.exit(1)

targetFile = open(sys.argv[1], 'w')
targetNumFracs = 10 # target number of fractures
b = 20              # width of the domain
h = 20              # height of the domain
r = 1.4             # tunnel radius
cr = 0.7            # canister radius
d = 2.0*r           # horizontal distance between fracture ends and origin
i = 2.5*d           # horizontal distance between 'intersection point' of fractures and origin

v = 2*r/(targetNumFracs/2+1) #vertical distance between first points

fractures = []

while len(fractures) < targetNumFracs:

    # first point
    y = -r+v+len(fractures)/2*v+r/100
    x = math.sqrt(r**2-y**2)
    p1 = np.array([x,y])

    # second point
    y2 = y-y/(i-x)*d
    p2 = np.array([d,y2])

    # segment
    segment = [p1, p2]

    # an admissile segment has been found
    fractures.append(segment)
    print("Found admissible fracture number " + str(len(fractures)))

    #reflection
    x *=-1
    p1 = np.array([x,y])
    p2 = np.array([-d,y2])
    segment = [p1, p2]
    fractures.append(segment)
    print("Found admissible fracture number " + str(len(fractures)))

# write the remainder of the .geo file
targetFile.write("SetFactory(\"OpenCASCADE\"); // Use OpenCascade for boolean operations\n")
targetFile.write("Mesh.Format = 1;             // Use Gmsh mesh format\n\n")

targetFile.write("/////////////////////\n")
targetFile.write("// Inputs\n")

targetFile.write("x = " + str(b)   + "; // x extent of the domain (in meter)\n")
targetFile.write("y = " + str(h)   + "; // y extent of the domain (in meter)\n")
targetFile.write("r = " + str(r)   + "; // tunnel radius (in meter)\n")
targetFile.write("cr = " + str(cr) + "; // canister radius (in meter)\n")

targetFile.write("size_f = " + str(r/3.0) + "; // mesh size on fractures\n")
targetFile.write("size_t = " + str(r/2.0) + "; // mesh size inside the tunnel\n")
targetFile.write("size_d = " + str(r) + ";     // mesh size in domain\n\n")

# write the generated lines into the .geo file
for i in range(0, len(fractures)):
    p1 = fractures[i][0]
    p2 = fractures[i][1]
    p1Idx = i*2 + 1
    p2Idx = i*2 + 2
    targetFile.write("Point(" + str(p1Idx) + ") = {" + str(p1[0]) + ", " + str(p1[1]) + ", 0.0};\n")
    targetFile.write("Point(" + str(p2Idx) + ") = {" + str(p2[0]) + ", " + str(p2[1]) + ", 0.0};\n")
    targetFile.write("Line(" + str(i+1) + ") = {" + str(p1Idx) + ", " + str(p2Idx) + "};\n\n")

targetFile.write("///////////////////////////////////////\n")
targetFile.write("// Process input into final geometry\n\n")

targetFile.write("// construct the actual domain by cutting out the tunnel from a rectangle\n")
targetFile.write("rectId   = news; Rectangle(rectId) = {-x/2.0, -y/2.0, 0, x, y, 0}; // domain outline\n")
targetFile.write("tunnelId = news; Disk(tunnelId) = {0, 0, 0, r, r};                 // disk desribing the tunnel\n")
targetFile.write("domainId = news; BooleanDifference(domainId) = { Surface{rectId}; Delete; }{ Surface{tunnelId}; };\n\n")
targetFile.write("canisterId = news; Disk(canisterId) = {0, 0, 0, cr, cr};           // disk desribing the canister\n")
targetFile.write("hollowTunnelId = news; BooleanDifference(hollowTunnelId) = { Surface{tunnelId}; Delete; }{ Surface{canisterId}; Delete; };\n\n")

targetFile.write("// cut all lines to be confined by the domain\n")
targetFile.write("confinedLines[] = BooleanIntersection{ Line{1:" + str(len(fractures)) + "}; Delete; }{ Surface{domainId}; };\n\n")

targetFile.write("// fragment the resulting lines to detect mutual intersections\n")
targetFile.write("fragmentedLines[] = BooleanFragments{ Line{confinedLines[]}; Delete; }{ Line{confinedLines[]}; Delete; };\n\n")

targetFile.write("// make the union of the tunnel and the domain\n")
targetFile.write("domainSurfs[] = BooleanUnion{ Surface{domainId}; Delete; }{ Surface{hollowTunnelId}; Delete; };\n\n")

targetFile.write("// fragment the domain by the lines\n")
targetFile.write("BooleanFragments{ Surface{domainSurfs[]}; Delete; }{ Line{fragmentedLines[]}; }\n\n")

targetFile.write("// set mesh size specs\n")
targetFile.write("Characteristic Length{ Point{:} } = size_d;\n")
targetFile.write("Characteristic Length{ PointsOf{Surface{hollowTunnelId};} } = size_t;\n\n")
targetFile.write("Characteristic Length{ PointsOf{Line{fragmentedLines[]};} } = size_f;\n\n")

targetFile.write("/////////////////////////\n")
targetFile.write("// set physical indices\n")
targetFile.write("If (gridDim == 2) // omit 2d elements if 1d grid is desired\n")
targetFile.write("    Physical Surface(2) = {Surface{:}};\n")
targetFile.write("EndIf\n")
targetFile.write("Physical Line(2) = {fragmentedLines[]};\n")
