import numpy as np
import math
import random
import sys

# function to determine if two lines intersect
def doIntersect(segment1, segment2):

    p11 = segment1[0]
    p12 = segment1[1]
    p21 = segment2[0]
    p22 = segment2[1]

    #slope of segments
    slope1 = (p11[1]-p12[1])/(p11[0]-p12[0])
    slope2 = (p21[1]-p22[1])/(p21[0]-p22[0])

    #parallel segments
    if(slope1 == slope2):
        return False

    #ordinates
    b1 = p11[1]-p11[0]*slope1
    b2 = p21[1]-p21[0]*slope2

    #intersection point
    Xi = (b2-b1)/(slope1-slope2)
    Yi = slope1 * Xi +b1

    #determine if lines intersect
    if ( (Xi < max( min(p11[0],p12[0]), min(p21[0],p22[0]) )) or
        (Xi > min( max(p11[0],p12[0]), max(p21[0],p22[0]) )) ):
        return False
    else:
        return slope1, slope2

# function to compute the intersection angle between two segments
def intersectionAngle(segment1, segment2, slope1, slope2):

    #compute intersection angle
    phi = math.degrees(math.atan((slope2-slope1)/(1+(slope2*slope1))))
    return abs(phi)

# function to compute the distance of a point to a segment
def computeDistance(segment, point):

    # project the point on the supporting line
    v1 = segment[1] - segment[0]
    v2 = point - segment[0]

    sp = v1.dot(v2)
    normV1 = np.linalg.norm(v1)

    # projection lies "behind" segment[0]
    if sp < 0.0:
        return np.linalg.norm(v2)

    # length on line from segment[0] to projection point
    length = sp/normV1
    if length > normV1: # projection lies outside the segment
        return np.linalg.norm(point - segment[1])

    # compute projection point
    v1 *= length/normV1
    projection = segment[0] + v1
    return np.linalg.norm(point - projection)

###########################
# main body of the script #
###########################

# get file name to write the result in
if len(sys.argv) != 2:
    sys.stderr.write("Please provide input argument - the target .geo file")
    sys.exit(1)

targetFile = open(sys.argv[1], 'w')
targetNumFracs = 15 # target number of fractures
b = 20              # width of the domain
h = 20              # height of the domain
r = 1.4             # tunnel radius
cr = 0.7            # canister radius
flMin = r           # minimum fracture length
flMax = 2.0*r       # maximum fracture length

minIntersectionAngle = math.degrees(math.pi/10.0) # minimum allowed intersection angle
minDistance = r*0.05               # minimum distance allowed

fractures = []
while len(fractures) < targetNumFracs:

    keepSampling = True
    while keepSampling:
        # first point (at tunnel)
        phi = random.uniform(0.0, math.pi*2.0)
        x = math.cos(phi)*r
        y = math.sin(phi)*r
        p1 = np.array([x, y])

        # second point
        # sample candidate for p2
        if abs(x) < 1e-6 or abs(y) < 1e-6:

            x2 = x + random.choice([-1, 1])*random.uniform(flMin, flMax)
            y2 = y
            p2 = np.array([x2, y2])

        else: # avoid segments passing through circle
            slopeAtP1 = abs(x/y)
            slope = random.uniform(0.0, slopeAtP1)

            # normalized sample direction vector
            dir = np.array([np.sign(x), -1.0*np.sign(y)*slope])
            dir /= np.linalg.norm(dir)

            # sample candidate
            p2 = p1 + dir*random.uniform(flMin, flMax)

        # segment candidate
        segment = [p1, p2]

        # check constraints
        keepSampling = False

        # avoid outliers
        if abs(p2[1])>r:
            keepSampling = True
            continue

        # avoid too short segments
        if np.linalg.norm(p2 - p1) < flMin:
            keepSampling = True
            continue

        # constraints to other fracture segments
        for fracSegment in fractures:

            # avoid small angles
            if doIntersect(fracSegment, segment) != False:
                slope1,slope2 = doIntersect(fracSegment, segment)
                if (intersectionAngle(fracSegment, segment, slope1, slope2) < minIntersectionAngle):
                    keepSampling = True
                    break

            # avoid small distances
            if (computeDistance(segment, fracSegment[0]) < minDistance
                or computeDistance(segment, fracSegment[1]) < minDistance
                or computeDistance(fracSegment, segment[0]) < minDistance
                or computeDistance(fracSegment, segment[1]) < minDistance):
                keepSampling = True
                break

    # an admissile segment has been found
    fractures.append(segment)
    print("Found admissible fracture number " + str(len(fractures)))

# write the remainder of the .geo file
targetFile.write("SetFactory(\"OpenCASCADE\"); // Use OpenCascade for boolean operations\n")
targetFile.write("Mesh.Format = 1;             // Use Gmsh mesh format\n\n")

targetFile.write("/////////////////////\n")
targetFile.write("// Inputs\n")

targetFile.write("x = " + str(b)   + "; // x extent of the domain (in meter)\n")
targetFile.write("y = " + str(h)   + "; // y extent of the domain (in meter)\n")
targetFile.write("r = " + str(r)   + "; // tunnel radius (in meter)\n")
targetFile.write("cr = " + str(cr) + "; // canister radius (in meter)\n")

targetFile.write("size_f = " + str(r/3.0) + "; // mesh size on fractures\n")
targetFile.write("size_t = " + str(r/2.0) + "; // mesh size inside the tunnel\n")
targetFile.write("size_d = " + str(r) + ";     // mesh size in domain\n\n")

# write the generated lines into the .geo file
for i in range(0, len(fractures)):
    p1 = fractures[i][0]
    p2 = fractures[i][1]
    p1Idx = i*2 + 1
    p2Idx = i*2 + 2
    targetFile.write("Point(" + str(p1Idx) + ") = {" + str(p1[0]) + ", " + str(p1[1]) + ", 0.0};\n")
    targetFile.write("Point(" + str(p2Idx) + ") = {" + str(p2[0]) + ", " + str(p2[1]) + ", 0.0};\n")
    targetFile.write("Line(" + str(i+1) + ") = {" + str(p1Idx) + ", " + str(p2Idx) + "};\n\n")

targetFile.write("///////////////////////////////////////\n")
targetFile.write("// Process input into final geometry\n\n")

targetFile.write("// construct the actual domain by cutting out the tunnel from a rectangle\n")
targetFile.write("rectId   = news; Rectangle(rectId) = {-x/2.0, -y/2.0, 0, x, y, 0}; // domain outline\n")
targetFile.write("tunnelId = news; Disk(tunnelId) = {0, 0, 0, r, r};                 // disk desribing the tunnel\n")
targetFile.write("domainId = news; BooleanDifference(domainId) = { Surface{rectId}; Delete; }{ Surface{tunnelId}; };\n\n")
targetFile.write("canisterId = news; Disk(canisterId) = {0, 0, 0, cr, cr};           // disk desribing the canister\n")
targetFile.write("hollowTunnelId = news; BooleanDifference(hollowTunnelId) = { Surface{tunnelId}; Delete; }{ Surface{canisterId}; Delete; };\n\n")

targetFile.write("// cut all lines to be confined by the domain\n")
targetFile.write("confinedLines[] = BooleanIntersection{ Line{1:" + str(len(fractures)) + "}; Delete; }{ Surface{domainId}; };\n\n")

targetFile.write("// fragment the resulting lines to detect mutual intersections\n")
targetFile.write("fragmentedLines[] = BooleanFragments{ Line{confinedLines[]}; Delete; }{ Line{confinedLines[]}; Delete; };\n\n")

targetFile.write("// make the union of the tunnel and the domain\n")
targetFile.write("domainSurfs[] = BooleanUnion{ Surface{:}; Delete; }{ Surface{:}; Delete; };\n\n")

targetFile.write("// fragment the domain by the lines\n")
targetFile.write("BooleanFragments{ Surface{domainSurfs[]}; Delete; }{ Line{fragmentedLines[]}; }\n\n")

targetFile.write("// set mesh size specs\n")
targetFile.write("Characteristic Length{ Point{:} } = size_d;\n")
targetFile.write("Characteristic Length{ PointsOf{Surface{hollowTunnelId};} } = size_t;\n\n")
targetFile.write("Characteristic Length{ PointsOf{Line{fragmentedLines[]};} } = size_f;\n\n")

targetFile.write("/////////////////////////\n")
targetFile.write("// set physical indices\n")
targetFile.write("If (gridDim == 2) // omit 2d elements if 1d grid is desired\n")
targetFile.write("    Physical Surface(2) = {Surface{:}};\n")
targetFile.write("EndIf\n")
targetFile.write("Physical Line(2) = {fragmentedLines[]};\n")
