import numpy as np
import math
import random
import sys

# get file name to write the result in
if len(sys.argv) != 2:
    sys.stderr.write("Please provide input argument - the target .geo file")
    sys.exit(1)

targetFile = open(sys.argv[1], 'w')
targetNumFracs = 4  # target number of fractures
b = 10              # width of the domain
h = 10              # height of the domain
r = 1.4             # tunnel radius
cr = 0.7            # canister radius
d = 1.5             # fractures length
i = 2.5*d           # horizontal distance between 'intersection point' of fractures and origin

v = 2*r/(2*targetNumFracs+1) #vertical distance between first points

fractures = []

while len(fractures) < targetNumFracs:

    # first point
    y = v/2+len(fractures)*v
    x = math.sqrt(r**2-y**2)
    p1 = np.array([x,y])

    # second point
    slope = (0-y)/(i-x)
    dir = np.array([np.sign(x), slope])
    dir /= np.linalg.norm(dir)
    p2 = p1 + dir*d

    # segment
    segment = [p1, p2]

    # an admissile segment has been found
    fractures.append(segment)
    print("Found admissible fracture number " + str(len(fractures)))


# write the remainder of the .geo file
targetFile.write("SetFactory(\"OpenCASCADE\"); // Use OpenCascade for boolean operations\n")
targetFile.write("Mesh.Format = 1;             // Use Gmsh mesh format\n\n")

targetFile.write("/////////////////////\n")
targetFile.write("// Inputs\n")

targetFile.write("x = " + str(b)   + "; // x extent of the domain (in meter)\n")
targetFile.write("y = " + str(h)   + "; // y extent of the domain (in meter)\n")
targetFile.write("r = " + str(r)   + "; // tunnel radius (in meter)\n")
targetFile.write("cr = " + str(cr) + "; // canister radius (in meter)\n")

targetFile.write("size_f = " + str(r/3.0) + "; // mesh size on fractures\n")
targetFile.write("size_t = " + str(r/2.0) + "; // mesh size inside the tunnel\n")
targetFile.write("size_d = " + str(r) + ";     // mesh size in domain\n\n")

# write the generated lines into the .geo file
for i in range(0, len(fractures)):
    p1 = fractures[i][0]
    p2 = fractures[i][1]
    p1Idx = i*2 + 1
    p2Idx = i*2 + 2
    targetFile.write("Point(" + str(p1Idx) + ") = {" + str(p1[0]) + ", " + str(p1[1]) + ", 0.0};\n")
    targetFile.write("Point(" + str(p2Idx) + ") = {" + str(p2[0]) + ", " + str(p2[1]) + ", 0.0};\n")
    targetFile.write("Line(" + str(i+1) + ") = {" + str(p1Idx) + ", " + str(p2Idx) + "};\n\n")

targetFile.write("///////////////////////////////////////\n")
targetFile.write("// Process input into final geometry\n\n")

targetFile.write("// describing the tunnel\n\n")
targetFile.write("p1 = newp; Point(p1) = {0.0, 0.0, 0.0};\n")
targetFile.write("p2 = newp; Point(p2) = {r, 0.0, 0.0};\n")
targetFile.write("p3 = newp; Point(p3) = {0.0, r, 0.0};\n\n")

targetFile.write("l1 = newl; Line(l1) = {p1, p2};\n")
targetFile.write("l2 = newl; Line(l2) = {p3, p1};\n")
targetFile.write("tunnel = newc; Circle(tunnel) = {p2, p1, p3};\n")
targetFile.write("ll1 = newll; Line Loop(ll1) = {l1, tunnel, l2};\n")
targetFile.write("tunnelId = news; Plane Surface(tunnelId)={ll1};\n\n")

targetFile.write("//describing the canisiter\n")
targetFile.write("p4 = newp; Point(p4) = {cr, 0.0, 0.0};\n")
targetFile.write("p5 = newp; Point(p5) = {0, cr, 0};\n\n")

targetFile.write("l3 = newl; Line(l3) = {p1,p4};\n")
targetFile.write("l4 = newl; Line(l4) = {p5,p1};\n")
targetFile.write("canister = newc; Circle(canister) = {p4,p1,p5};\n")
targetFile.write("ll2 = newll; Line Loop(ll2) = {l3, canister, l4};\n")
targetFile.write("canisterId = news; Plane Surface(canisterId)={ll2};\n\n")

targetFile.write("// construct the actual domain by cutting out the tunnel from a rectangle\n")
targetFile.write("rectId   = news; Rectangle(rectId) = {0, 0, 0, x, y, 0}; // domain outline\n")
targetFile.write("domainId = news; BooleanDifference(domainId) = { Surface{rectId}; Delete; }{ Surface{tunnelId}; };\n\n")

targetFile.write("hollowTunnelId = news; BooleanDifference(hollowTunnelId) = { Surface{tunnelId}; Delete; }{ Surface{canisterId}; Delete; };\n\n")

targetFile.write("// cut all lines to be confined by the domain\n")
targetFile.write("confinedLines[] = BooleanIntersection{ Line{1:" + str(len(fractures)) + "}; Delete; }{ Surface{domainId}; };\n\n")

targetFile.write("// fragment the resulting lines to detect mutual intersections\n")
targetFile.write("fragmentedLines[] = BooleanFragments{ Line{confinedLines[]}; Delete; }{ Line{confinedLines[]}; Delete; };\n\n")

targetFile.write("// make the union of the tunnel and the domain\n")
targetFile.write("domainSurfs[] = BooleanUnion{ Surface{domainId}; Delete; }{ Surface{hollowTunnelId}; Delete; };\n\n")

targetFile.write("// fragment the domain by the lines\n")
targetFile.write("BooleanFragments{ Surface{domainSurfs[]}; Delete; }{ Line{fragmentedLines[]}; }\n\n")

targetFile.write("// set mesh size specs\n")
targetFile.write("Characteristic Length{ Point{:} } = size_d;\n")
targetFile.write("Characteristic Length{ PointsOf{Surface{hollowTunnelId};} } = size_t;\n\n")
targetFile.write("Characteristic Length{ PointsOf{Line{fragmentedLines[]};} } = size_f;\n\n")

targetFile.write("/////////////////////////\n")
targetFile.write("// set physical indices\n")
targetFile.write("If (gridDim == 2) // omit 2d elements if 1d grid is desired\n")
targetFile.write("    Physical Surface(2) = {Surface{:}};\n")
targetFile.write("EndIf\n")
targetFile.write("Physical Line(2) = {fragmentedLines[]};\n")
