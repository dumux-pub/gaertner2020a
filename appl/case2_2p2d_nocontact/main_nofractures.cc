// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Test for the elastic two-phase model.
 *        the deformations and a contact problem on the fracture facets.
 * \note This test uses finite elements for the mechanical deformations and
 *       the box scheme for flow.
 */
#include <config.h>
#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

// the type tags of the sub-problems
#include "properties_bulk_poromech.hh"
#include "properties_bulk_twop.hh"

#include <dumux/io/grid/gridmanager.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/geomechanics/poroelastic/couplingmanager.hh>

#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>
#include <dumux/multidomain/fvgridgeometry.hh>
#include <dumux/multidomain/fvproblem.hh>
#include <dumux/multidomain/fvgridvariables.hh>

#include <dumux/multidomain/io/vtkoutputmodule.hh>

// The type tags of the bulk and facet flow problems
using BulkFlowTypeTag = Dumux::Properties::TTag::TwoPBulkBox;

// obtain/define some types to be used below in the property definitions and in main
class TestTraits
{
public:
    using MDTraits = Dumux::MultiDomainTraits< BulkFlowTypeTag,
                                               Dumux::Properties::TTag::PoroElasticBulkFem >;
    using CouplingManager = Dumux::PoroMechanicsCouplingManager<MDTraits>;
};

// set the coupling manager property in the sub-problems
namespace Dumux {
namespace Properties {

template<class TypeTag>
struct CouplingManager<TypeTag, BulkFlowTypeTag> { using type = typename TestTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::PoroElasticBulkFem> { using type = typename TestTraits::CouplingManager; };

} // end namespace Properties
} // end namespace Dumux

///////////////////
// main function //
///////////////////
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    Dune::MPIHelper::instance(argc, argv);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // the multidomain traits and domain indices
    using Traits = typename TestTraits::MDTraits;
    using CouplingManager = typename TestTraits::CouplingManager;
    constexpr auto bulkFlowId = Traits::SubDomain<0>::Index();
    constexpr auto bulkMechId = Traits::SubDomain<1>::Index();

    ///////////////////////////////////
    // Create the grid used for flow //
    ///////////////////////////////////
    using Grid = GetPropType<BulkFlowTypeTag, Properties::Grid>;
    using GridView = typename Grid::LeafGridView;
    using GridManager = GridManager<Grid>;
    GridManager gridManager;
    gridManager.init();
    gridManager.loadBalance();

    const auto& bulkGridView = gridManager.grid().leafGridView();

    ///////////////////////////////////////////////////////
    // All grids are built, make the grid geometries now //
    ///////////////////////////////////////////////////////
    using MDGridGeometry = MultiDomainFVGridGeometry<Traits>;
    MDGridGeometry fvGridGeometry;

    // standard construction for bulkFlow and facet flow
    fvGridGeometry.set(std::make_shared<typename MDGridGeometry::template Type<bulkFlowId>>(bulkGridView), bulkFlowId);

    // FEM-type construction for mechanical sub-domain
    using MechSpaceBasis = typename MDGridGeometry::template Type<bulkMechId>::AnsatzSpaceBasis;
    auto mechSpaceBasis = std::make_shared<MechSpaceBasis>(bulkGridView);
    fvGridGeometry.set(std::make_shared<typename MDGridGeometry::template Type<bulkMechId>>(mechSpaceBasis), bulkMechId);

    // update the grid geometries
    fvGridGeometry[bulkMechId].update();
    fvGridGeometry[bulkFlowId].update();

    // the coupling manager
    using CouplingManager = typename TestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();


    ////////////////////////////////////
    // Instantiate the (sub-)problems //
    ////////////////////////////////////
    MultiDomainFVProblem<Traits> problem;

    using BulkFlowProblem = MultiDomainFVProblem<Traits>::template Type<bulkFlowId>;
    using BulkElasticProblem = MultiDomainFVProblem<Traits>::template Type<bulkMechId>;

    auto bulkFlowSpatialParams = std::make_shared<typename BulkFlowProblem::SpatialParams>(fvGridGeometry.get(bulkFlowId), couplingManager, "OnePBulk");
    auto bulkElasticSpatialParams = std::make_shared<typename BulkElasticProblem::SpatialParams>(fvGridGeometry.get(bulkMechId), "ElasticBulk");

    problem.set(std::make_shared<BulkFlowProblem>(fvGridGeometry.get(bulkFlowId), bulkFlowSpatialParams, couplingManager, "OnePBulk"), bulkFlowId);
    problem.set(std::make_shared<BulkElasticProblem>(fvGridGeometry.get(bulkMechId), bulkElasticSpatialParams, couplingManager, "ElasticBulk"), bulkMechId);

    // the solution vector
    typename Traits::SolutionVector x;

    // initialize the coupling manager, it receives all problems and the coupling maps!
    couplingManager->init(problem.get(bulkFlowId),
                          problem.get(bulkMechId),
                          x);

    // initialize the solution vector
    problem.applyInitialSolution(x);
    auto xOld = x;

    // initialize the grid variables with the initial solution
    using GridVariables = MultiDomainFVGridVariables<Traits>;
    GridVariables gridVars(fvGridGeometry.getTuple(), problem.getTuple());
    gridVars.init(x);

    ///////////////////////////
    // Initialize VTK output //
    ///////////////////////////

    using BulkFlowGridVariables = typename GridVariables::template Type<bulkFlowId>;
    using BulkFlowVtkOutputModule = VtkOutputModule<BulkFlowGridVariables, typename Traits::template SubDomain<bulkFlowId>::SolutionVector>;

    static constexpr bool bulkFlowIsBox = MDGridGeometry::template Type<bulkFlowId>::discMethod == DiscretizationMethod::box;
    const auto bulkOutputType = bulkFlowIsBox ? Dune::VTK::nonconforming : Dune::VTK::conforming;
    BulkFlowVtkOutputModule bulkFlowVtkWriter(gridVars[bulkFlowId], x[bulkFlowId], problem[bulkFlowId].name(), "", bulkOutputType);

    // add additional output
    using BulkFlowIOFields = GetPropType<typename Traits::template SubDomain<bulkFlowId>::TypeTag, Properties::IOFields>;
    problem[bulkFlowId].addOutputFields(bulkFlowVtkWriter);
    BulkFlowIOFields::initOutputModule(bulkFlowVtkWriter);

    //////////////////////////////////////
    // VTK output for mechanical domain
    using Vector = Dune::FieldVector<double, GridView::dimension>;
    using MechElementMapper = typename MDGridGeometry::template Type<bulkMechId>::ElementMapper;
    using P0MechanicsFunction = Vtk::VectorP0VTKFunction<GridView, MechElementMapper, std::vector<Vector>>;
    auto mechWriter = std::make_shared<Dune::VTKWriter<GridView>>(bulkGridView);
    Dune::VTKSequenceWriter<GridView> mechSequenceWriter(mechWriter, getParamFromGroup<std::string>("ElasticBulk", "Problem.Name"));

    auto gfU = Dune::Functions::makeDiscreteGlobalBasisFunction<Vector>(*mechSpaceBasis, x[bulkMechId]);
    auto sigmaXFunction = std::make_shared<P0MechanicsFunction>(bulkGridView, fvGridGeometry[bulkMechId].elementMapper(), problem[bulkMechId].sigma_x(), "Sigma_x", 2);
    auto sigmaYFunction = std::make_shared<P0MechanicsFunction>(bulkGridView, fvGridGeometry[bulkMechId].elementMapper(), problem[bulkMechId].sigma_y(), "Sigma_y", 2);

    mechWriter->addVertexData(gfU, Dune::VTK::FieldInfo("u", Dune::VTK::FieldInfo::Type::vector, 2));
    mechWriter->addCellData(sigmaXFunction);
    mechWriter->addCellData(sigmaYFunction);

    ///////////////////////
    // Solve the problem //
    ///////////////////////

    const auto dt = getParam<double>("TimeLoop.DtInitial");
    const auto tEnd = getParam<double>("TimeLoop.TEnd");
    auto timeLoop = std::make_shared<TimeLoop<double>>(0.0, dt, tEnd);

    // the assembler
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( problem.getTuple(), fvGridGeometry.getTuple(), gridVars.getTuple(),
                                                  couplingManager, timeLoop, xOld);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

    // log files for pressure and time
    std::ofstream pressureFile("avgpressure.log", std::ios::out);

    timeLoop->start(); do
    {
        assembler->setPreviousSolution(xOld);
        couplingManager->setPreviousSolution(&xOld);

        ///////////////////////////
        // The actual solve step
        newtonSolver->solve(x, *timeLoop);

        gridVars.advanceTimeStep();
        timeLoop->advanceTimeStep();

        //////////////////////
        // Write VTK output //
        //////////////////////

        // update additional output
        problem[bulkFlowId].updateOutputFields(x[bulkFlowId]);

        // update stresses
        problem[bulkMechId].updateOutputFields(gridVars[bulkMechId], *assembler, x[bulkMechId], bulkMechId);

        // write vtk output
        bulkFlowVtkWriter.write(timeLoop->time());
        mechSequenceWriter.write(timeLoop->time());

        // print average pressure to screen & save to file
        const auto pAvg = problem[bulkFlowId].computeAveragePressure(x[bulkFlowId], gridVars[bulkFlowId]);
        std::cout << "Average pressures (pw/pn): " << pAvg[0] << ", " << pAvg[1] << std::endl;
        pressureFile << timeLoop->time() << "," << pAvg[0] << "," << pAvg[1] << std::endl;

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the newton solver
        timeLoop->setTimeStepSize(newtonSolver->suggestTimeStepSize(timeLoop->timeStepSize()));

        // the new solution will be the old one in the next time step
        xOld = x;
    } while (!timeLoop->finished());

    // print parameter usage and exit
    Parameters::print();
    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (std::exception& e)
{
    std::cerr << "Standard exception: " << e.what() << " ---> Abort!" << std::endl;
    return 4;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 5;
}
