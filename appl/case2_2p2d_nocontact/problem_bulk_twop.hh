// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the bulk domain in the elastic
 *        two-phase facet coupling test.
 */
#ifndef DUMUX_GAERTNER2020_CASE2_BULK_FLOW_PROBLEM_HH
#define DUMUX_GAERTNER2020_CASE2_BULK_FLOW_PROBLEM_HH

#include <dune/geometry/quadraturerules.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/discretization/evalsolution.hh>

namespace Dumux {

/*!
 * \brief The problem for the bulk domain in the elastic
 *        two-phase facet coupling test.
 */
template<class TypeTag>
class TwoPBulkProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    //! The constructor
    TwoPBulkProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                    std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                    std::shared_ptr<CouplingManager> couplingManagerPtr,
                    const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManagerPtr)
    {
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        gasProductionRate_ = getParam<Scalar>("Problem.GasProductionRate");
        porosity_.resize(fvGridGeometry->gridView().size(0), getParam<Scalar>("SpatialParams.InitialPorosity"));
        time_ = 0;
    }

    //! The problem name.
    const std::string& name() const
    { return problemName_; }

    //! Specifies the kind of boundary condition on a given boundary position.
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        // we simulate the upper right quarter of the domain
        // -> use Dirichlet on the top and right boundaries, no-flow elsewhere
        // use Dirichlet on all outer boundaries
        if (globalPos[0] > this->gridGeometry().bBoxMax()[0] - 1e-6
            || globalPos[1] > this->gridGeometry().bBoxMax()[1] - 1e-6)
            values.setAllDirichlet();

        return values;
    }

    //! Specifies which kind of interior boundary condition to use at fracture facets
    BoundaryTypes interiorBoundaryTypes(const Element& element,
                                        const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        values.setAllDirichlet();
        return values;
    }

    //! Evaluates the Dirichlet boundary conditions at a given position.
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return initialAtPos(globalPos); }

    //! evaluate the Neumann boundary conditions
    //! Evaluate the boundary conditions for a neumann boundary segment.
    template<class ElemVolVars, class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElemVolVars& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        static const Scalar canisterRadius = getParam<Scalar>("Problem.CanisterRadius");
        const Scalar waterConsumptionRate_ = (gasProductionRate_ / 0.1) * (18.015e-3/0.02479);
        // we assume the lower left corner to be at x = y = 0
        if ( scvf.ipGlobal().two_norm() < canisterRadius + 1e-6 && time_ < 1e12 + 1e3)
        return NumEqVector({1.0*waterConsumptionRate_, -1.0*gasProductionRate_});
        return NumEqVector(0.0);
    }
    void setTime (Scalar t)
    {
        time_ = t;
    }

    //! Evaluates the initial conditions.
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        static const Scalar initP = getParam<Scalar>("Problem.InitialFluidPressure");
        return PrimaryVariables({initP, 0.0});
    }

    //! Returns the temperature in \f$\mathrm{[K]}\f$ in the domain.
    Scalar temperature() const
    { return 283.15; /*10°C*/ }

    //! Returns const reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! adds additional output fields to a vtk writer
    template<class OutputModule>
    void addOutputFields(OutputModule& outputModule)
    {
        outputModule.addField(porosity_, "porosity", OutputModule::FieldType::element);
    }

    //! updates the output fields for a given solution
    template<class SolutionVector>
    void updateOutputFields(const SolutionVector& x)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, x, this->gridGeometry());
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            // compute an aveage porosity on the element
            Scalar porosity = 0.0;
            for (const auto& scv : scvs(fvGeometry))
                porosity += scv.volume()*this->spatialParams().porosity(element, scv, elemSol);
            porosity /= element.geometry().volume();

            const auto eIdx = this->gridGeometry().elementMapper().index(element);
            porosity_[eIdx] = porosity;
        }
    }

    //! computes the average pressure at the canister
    template<class SolutionVector, class GridVars>
    std::array<Scalar, 2> computeAveragePressure(const SolutionVector& sol, const GridVars& gridVars) const
    {
        Scalar avgPw = 0.0;
        Scalar avgPn = 0.0;
        Scalar intVolume = 0.0;
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto eg = element.geometry();
            const auto elemSol = elementSolution(element, sol, this->gridGeometry());
            auto elemSolPn = elemSol;

            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());
            fvGeometry.bindElement(element);
            elemVolVars.bindElement(element, fvGeometry, sol);

            for (const auto& scv : scvs(fvGeometry))
                elemSolPn[scv.localDofIndex()] = elemVolVars[scv].pressure(/*nPhaseIdx*/1);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                // the scvfs at the canister are those that have Neumann BCs
                static const Scalar canisterRadius = getParam<Scalar>("Problem.CanisterRadius");
                if (scvf.boundary() && scvf.ipGlobal().two_norm() < canisterRadius + 1e-6)
                {
                    const auto& geometry = scvf.geometry();
                    const auto& rule = Dune::QuadratureRules<Scalar, GridView::dimension-1>::rule(geometry.type(), 2);

                    for (const auto& quadPoint : rule)
                    {
                        const auto& localPos = quadPoint.position();
                        const auto globalPos = geometry.global(localPos);

                        const auto intElement = geometry.integrationElement(localPos);
                        const Scalar pw = evalSolution(element, eg, this->gridGeometry(), elemSol, globalPos)[0];
                        const Scalar pn = evalSolution(element, eg, this->gridGeometry(), elemSolPn, globalPos)[0];

                        const auto weight = intElement*quadPoint.weight();
                        avgPw += pw*weight;
                        avgPn += pn*weight;
                    }

                    intVolume += geometry.volume();
                }
            }
        }

        return {avgPw/intVolume, avgPn/intVolume};
    }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    std::string problemName_;
    Scalar gasProductionRate_;
    std::vector<Scalar> porosity_;
    Scalar time_;
};

} // end namespace Dumux

#endif
