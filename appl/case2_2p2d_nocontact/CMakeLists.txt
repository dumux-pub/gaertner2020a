dune_symlink_to_source_files(FILES "params.input" "runandplot.py" "runandplot_p_n.py")
dune_add_test(NAME case2_2p2d_nocontact
              SOURCES main.cc
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )")
dune_add_test(NAME case2_2p2d_nofractures
              SOURCES main_nofractures.cc
              COMPILE_DEFINITIONS FRACTURESCONSIDERED=0
              CMAKE_GUARD "( dune-alugrid_FOUND )")
set(CMAKE_BUILD_TYPE Release)
