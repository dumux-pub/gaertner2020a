// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the lagrange-multiplier domain that is
 *        used to model contact mechanics of closing fractures.
 */
#ifndef DUMUX_GAERTNER2020_CASE1_CONTACT_PROBLEM_HH
#define DUMUX_GAERTNER2020_CASE1_CONTACT_PROBLEM_HH

#include <dumux/common/feproblem.hh>

namespace Dumux {

/*!
 * \brief The problem for the (d-1)-dimensional lagrange multiplier
 *        domain in the elastic single-phase facet coupling test.
 */
template<class TypeTag>
class ContactProblem : public FEProblem<TypeTag>
{
    using ParentType = FEProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using SecondaryVariables = typename GridVariables::SecondaryVariables;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Grid = typename GridGeometry::GridView::Grid;
    using FEElementGeometry = typename GridGeometry::LocalView;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    static constexpr int numEq = NumEqVector::size();
    static constexpr int dimWorld = GridView::dimensionworld;

public:
    ContactProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                   std::shared_ptr<CouplingManager> couplingManagerPtr,
                   const std::string& paramGroup = "")
    : ParentType(gridGeometry, paramGroup)
    , couplingManagerPtr_(couplingManagerPtr)
    {
        problemName_  =  getParam<std::string>("Vtk.OutputName")
                         + "_"
                         + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        initialValues_ = getParamFromGroup<NumEqVector>(paramGroup, "InitialTraction");
        penaltyFactor_ = getParamFromGroup<Scalar>(paramGroup, "PenaltyFactor");
        frictionCoefficient_ = getParamFromGroup<Scalar>(paramGroup, "FrictionCoefficient");
        initialGap_ = getParam<Scalar>("Fractures.InitialGap");
    }

    //! The problem name.
    const std::string& name() const
    { return problemName_; }

    //! Specifies the kind of boundary condition at a boundary position.
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    //! Evaluates the interface constraint
    template<class ElementSolution, class IpData>
    NumEqVector evalConstraint(const Element& element,
                               const FEElementGeometry& feGeometry,
                               const ElementSolution& elemSol,
                               const IpData& ipData,
                               const SecondaryVariables& secVars) const
    {
        using std::max;
        NumEqVector result;

        const auto& traction = secVars.priVars();
        const auto& contactSurfaceSegment = couplingManager().getContactSurfaceSegment(element);

        const auto a = couplingManager().computeAperture(element, ipData.ipGlobal(), initialGap_);
        const auto deltaUT = couplingManager().computeTangentialDisplacementJump(element, ipData.ipGlobal());

        // compute normal and tangential traction
        const auto tN = traction*contactSurfaceSegment.getBasisVector(dimWorld-1);
        auto tT = contactSurfaceSegment.getBasisVector(dimWorld-1);
        tT *= -1.0*tN;
        tT += traction;

        const auto normalMaxArg = -1.0*tN - penaltyFactor_*a;
        auto tangMaxArg = deltaUT;
        tangMaxArg *= penaltyFactor_;
        tangMaxArg -= tT;

        const auto tangMaxArgNorm = tangMaxArg.two_norm();
        const auto& tangent = contactSurfaceSegment.getBasisVector(0);
        const auto fricCoeff = frictionCoefficient(element, ipData.ipGlobal());

        result[0] = -1.0*(tT*tangent)*max(fricCoeff*normalMaxArg, tangMaxArgNorm)
                    -fricCoeff*max(0.0, normalMaxArg)*(tangMaxArg*tangent);
        result[numEq-1] = -1.0*tN - max(0.0, normalMaxArg);

        return result;
    }

    //! Evaluates the initial conditions.
    PrimaryVariables initial(const Element& element, const GlobalPosition& globalPos) const
    { return initialValues_; }

    //! Evaluates the Dirichlet boundary conditions.
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { DUNE_THROW(Dune::InvalidStateException, "Should not have Dirichlet BCs"); }

    //! Returns const reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! Returns the friction coefficient at a given position
    const Scalar frictionCoefficient(const Element& element, const GlobalPosition& globalPos) const
    { return frictionCoefficient_; }

private:

    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    std::string problemName_;

    PrimaryVariables initialValues_;
    Scalar penaltyFactor_;
    Scalar frictionCoefficient_;
    Scalar initialGap_;
};

} // end namespace Dumux

#endif
