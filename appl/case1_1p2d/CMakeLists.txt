dune_symlink_to_source_files(FILES "params.input")
dune_add_test(NAME case1_1p2d
              SOURCES main.cc
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )")
set(CMAKE_BUILD_TYPE Release)
