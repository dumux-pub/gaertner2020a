// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the poromechanical domain in the elastic
 *        single-phase facet coupling test.
 */
#ifndef DUMUX_ANALYTIC_CRACK_BULK_POROELASTIC_PROBLEM_HH
#define DUMUX_ANALYTIC_CRACK_BULK_POROELASTIC_PROBLEM_HH

#include <dumux/geomechanics/problem.hh>

namespace Dumux {

/*!
 * \brief The problem for the poromechanical domain in the elastic
 *        single-phase facet coupling test.
 */
template<class TypeTag>
class PoroElasticSubProblem : public GeomechanicsProblem<TypeTag>
{
    using ParentType = GeomechanicsProblem<TypeTag>;

    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GGLocalView = typename GridGeometry::LocalView;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using StressType = GetPropType<TypeTag, Properties::StressType>;
    using Force = typename StressType::ForceVector;

public:
    PoroElasticSubProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                          std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                          std::shared_ptr<CouplingManager> couplingManagerPtr,
                          const std::string& paramGroup = "")
    : ParentType(gridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManagerPtr)
    {
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        // output fields
        sigma_x_.resize(gridGeometry->gridView().size(0), Force(0.0));
        sigma_y_.resize(gridGeometry->gridView().size(0), Force(0.0));
    }

    //! The problem name.
    const std::string& name() const
    { return problemName_; }

    //! Returns the temperature in the domain.
    static constexpr Scalar temperature()
    { return 273.15; }

    //! Evaluates the initial conditions for a given position.
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! Evaluates the boundary conditions for a given position.
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! Evaluate the boundary conditions for a neumann boundary segment.
    template<class ElementSolution, class IpData, class SecondaryVariables>
    NumEqVector neumann(const Element& element,
                        const Intersection& is,
                        const ElementSolution& elemSol,
                        const IpData& ipData,
                        const SecondaryVariables& secVars) const
    {
        PrimaryVariables values(0.0);

        // apply contact traction on interior boundaries
        // neglect facet pressure here!!
        if (couplingManager().isOnInteriorBoundary(element, is))
        {
            // Contribution of the fluid pressure
            const auto& facetVolVars = couplingManager().getLowDimVolVars(element, is, ipData.ipGlobal());
            Force force = is.centerUnitOuterNormal();
            force *= facetVolVars.pressure();
            force *= -1.0;

            // Contribution of the contact mechanics
            force += couplingManager().getContactTraction(element, is, ipData.ipGlobal());

            values = force;
        }
        // (maybe) inner boundary to tunnel
        else if (!isOnOuterBoundary_(ipData.ipGlobal()))
        {
            // apply current boundary pressure
            Force force = is.centerUnitOuterNormal();
            force *= couplingManager().getPMFlowVolVars(element, ipData.ipGlobal()).pressure();
            force *= -1.0;
        }

        return values;
    }

    //! Returns the effective fluid density.
    template<class IpData, class ElemSol>
    Scalar effectiveFluidDensity(const Element& element,
                                 const IpData& ipData,
                                 const ElemSol& elemSol) const
    { return couplingManager().getPMFlowVolVars(element, ipData.ipGlobal()).density(); }

    //! Returns the effective pore pressure (fem implementation).
    template< class IpData, class ElemSol, class SecondaryVariables >
    Scalar effectivePorePressure(const Element& element,
                                 const GGLocalView& feGeometry,
                                 const ElemSol& elemSol,
                                 const IpData& ipData,
                                 const SecondaryVariables& secVars) const
    { return couplingManager().getPMFlowVolVars(element, ipData.ipGlobal()).pressure(); }

    //! Specifies which kind of boundary condition should be
    //! used for which equation on a given boundary segment.
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        // use Dirichlet on all outer boundaries
        if (!isOnOuterBoundary_(globalPos))
            values.setAllDirichlet();

        return values;
    }

    //! Returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! update the output fields
    template<class GridVariables, class Assembler, class SolutionVector, class Id>
    void updateOutputFields(const GridVariables& gridVariables,
                            const Assembler& assembler,
                            const SolutionVector& x,
                            Id id)
    {
        using AnsatzBasis = typename GridGeometry::AnsatzSpaceBasis;
        using FiniteElement = typename AnsatzBasis::LocalView::Tree::FiniteElement;
        using LocalBasis = typename FiniteElement::Traits::LocalBasisType;

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto eIdx = this->gridGeometry().elementMapper().index(element);

            couplingManagerPtr_->bindCouplingContext(id, element, assembler);
            auto feGeometry = localView(this->gridGeometry());
            feGeometry.bind(element);

            const auto& eg = element.geometry();
            const auto elemSol = elementSolution(element, x, this->gridGeometry());
            const auto& localBasis = feGeometry.feBasisLocalView().tree().finiteElement().localBasis();

            FEIntegrationPointData<GlobalPosition, LocalBasis> ipData(eg, eg.local(eg.center()), localBasis);
            typename GridVariables::SecondaryVariables secVars;
            secVars.update(elemSol, *this, element, ipData);

            const auto sigma = StressType::effectiveStressTensor(*this, element, feGeometry, elemSol, ipData, secVars);
            sigma_x_[eIdx] = sigma[Indices::uxIdx];
            sigma_y_[eIdx] = sigma[Indices::uyIdx];
        }
    }

    //! return references to the stress tensor rows
    const std::vector<Force>& sigma_x() const { return sigma_x_; }
    const std::vector<Force>& sigma_y() const { return sigma_y_; }

private:
    bool isOnOuterBoundary_(const GlobalPosition& globalPos) const
    {
        return !(globalPos[0] < this->gridGeometry().bBoxMin()[0] + 1e-6
                 || globalPos[0] > this->gridGeometry().bBoxMax()[0] - 1e-6
                 || globalPos[1] < this->gridGeometry().bBoxMin()[1] + 1e-6
                 || globalPos[1] > this->gridGeometry().bBoxMax()[1] - 1e-6);
    }

    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    std::string problemName_;
    std::vector<Force> sigma_x_;
    std::vector<Force> sigma_y_;
};

} // end namespace Dumux

#endif
