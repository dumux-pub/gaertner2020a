This module works with the Dumux branch `feature/facet-compositional-geomechanics`.

TODO: Once the features on that branch are available on master, the module should be adjusted to work with Dumux master.

SUMMARY
=======
This is the DuMuX-pub module containing the code for reproducing the results submitted for:

Katharina Gärtner <br>
Numerical investigation of fracture dilation processes in radioactive waste storage sites<br>
Master's Thesis, 2020<br>
Universität Stuttgart

Installation
============

A simple way to install this module is to create a new folder and execute the file
[install_gaertner2020a.py](https://git.iws.uni-stuttgart.de/dumux-pub/gaertner2020a/-/raw/master/install_gaertner2020a.py)
in that folder.

```bash
mkdir -p Gaertner2020a && cd Gaertner2020a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/gaertner2020a/-/raw/master/install_gaertner2020a.py
python3 install_gaertner2020a.py
```
For an overview of the used versions of the DUNE and DuMuX modules, please have a look at
[install_gaertner2020a.py](https://git.iws.uni-stuttgart.de/dumux-pub/gaertner2020a/-/raw/master/install_gaertner2020a.py).


Installation with Docker
========================

Create a new folder and change into it

```bash
mkdir Gaertner2020a
cd Gaertner2020a
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/gaertner2020a/-/raw/master/docker_gaertner2020a.sh
```

Then open the docker container by running
```bash
bash docker_gaertner2020a.sh open
```

Reproducing the results
=======================

After the script has run successfully, executables can be built

```bash
cd DUMUX/gaertner2020a/build-cmake
make build_tests
```

and they are located in the build-cmake folder according to the following paths

- appl/case1_1p2d
- appl/case1_1p2d_nocontact
- appl/case2_2p2d_nocontact
- appl/case3_2pni2d_nocontact

They can be executed individually with an input file e.g., by running

```bash
cd appl/case1_1p2d
./case1_1p2d params.input
```
Note:- The executable case3_2pni2d_nocontact may not work. We suggest contacting the maintainer if it is really necessary to reproduce the corresponding result.
